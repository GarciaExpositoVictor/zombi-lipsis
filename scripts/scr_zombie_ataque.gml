sprite_index = spr_zombie_atacando;
image_speed = 0.25;
velocidad = 0;
direccion = point_direction(x, y, obj_jugador.x, obj_jugador.y);
//Creamos una instancia del ataque encima del jugador cuando se ejecute la 5ª subimagen
//del sprite del zombie atacando, que es cuando golpea
if(image_index == 5){
    instance_create(obj_jugador.x, obj_jugador.y, obj_ataque_zombie);
    audio_play_sound(snd_zombie_ataque, 70, false);
}
